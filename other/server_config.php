<?php

namespace main\s_r;

//This trait can be modified, it depends on the administration requirement
//This is the default version
trait server_config{
    protected $SITE_FROM_HOST_URI='/';
    protected $USE_PROTOCOL_NUMBER=80;
    protected $FILE_CONFIG='back/config';
    protected $FILE_TOOLS='back/tools';
    protected $DEFAULT_EXT='.php';
    public $DEFAULT_CHARSET='UTF-8';
    public $FILE_UPLOAD_DIR='upload';
    public $FILE_FRONT_VIEW='front';
    public $FILE_DOWNLOAD_DIR='download';
    public $MAINTENANCE_MODE=false;
}

?>
