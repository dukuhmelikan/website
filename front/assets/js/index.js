var winW=window.innerWidth;
var winH=window.innerHeight;

function scrollAdderY(){
    window.scroll(0,window.scrollY+window.innerHeight);
}

function scrollAnimation(){
    // Detect how far the scroll has been
    let scrollY=window.scrollY;
    
    // This animation is for landscape device only
    if(winW>winH){
        // Detecting visible element on scroll
        var elements=['produk','lokasi','kegiatan','footer'];
        let elementsY=[];
        for(var i=0;i<elements.length;i++){
            elementsY[i]=document.getElementsByClassName(elements[i])[0].getBoundingClientRect().y;    
        }
        for(var j=0;j<elementsY.length;j++){
            if(window.scrollY > Math.round(elementsY[j])+30){
                let targetElement=document.getElementsByClassName(elements[j])[0];
                // the animation is taking action
            
                // in which target this animation is
                if(j==0){
                    targetElement.childNodes[3].style.animation='left-to-center 2s linear 1s backwards';
                }else if(j==1){
                    targetElement.childNodes[3].style.animation='bottom-to-center 2s linear 2s backwards';
                    targetElement.childNodes[5].style.animation='bottom-to-center 2s linear 2s backwards';
                }else if(j==2){
                    targetElement.childNodes[3].childNodes[1].style.animation='right-to-center 2s linear 1s backwards';
                }else{
                    targetElement.childNodes[1].style.animation='left-to-center 2s linear 3s backwards';
                }
            }
        }
    }
}
window.onscroll=function(){
    scrollAnimation();
}
function redirect(){
    //Elements below are used to redirect
    // redirecting inside the html 
    var attrRedirectInside='in-redirect';
    var elementsTarget=document.querySelectorAll("["+attrRedirectInside+"]");
    for(var i=0;i<elementsTarget.length;i++){
        let chosenElement=elementsTarget[i];
        chosenElement.onclick=function(){
            let chosenAttr=chosenElement.getAttribute(attrRedirectInside);
            let chosenAttrN=chosenAttr.length;
            // if the content of the attribute has a [.] then it shall redirect to a class
            if(chosenAttr[0]=='.'){
                let targetClassString=[];
                for(var j=1;j<chosenAttrN;j++){
                    targetClassString[j-1]=chosenAttr[j];
                }
                var targetClass=targetClassString.join("");
                var targetClassElem=document.getElementsByClassName(targetClass);
                var targetPosY=targetClassElem[0].getBoundingClientRect().y;
                window.scroll(0,targetPosY);
            }
        }
    }
    // redirecting outside the html
    var attrRedirectOutside='ex-redirect';
    var elementsTargetEx=document.querySelectorAll('['+attrRedirectOutside+']');
    
    for(var j=0;j<elementsTargetEx.length;j++){
        let chosenElement=elementsTargetEx[j];
        chosenElement.onclick=function(){
            let chosenAttr=chosenElement.getAttribute(attrRedirectOutside);
            window.location.href=chosenAttr;
        }
    }
}
function imgMakeLazyLoad(){
    var img=document.getElementsByTagName('img');
    var imgN=img.length;
    for(var i=0;i<imgN;i++){
        var chosenImage=img[i];
        chosenImage.setAttribute('loading','lazy');
    }
}
window.onload=function(){
    imgMakeLazyLoad();
    redirect();
}