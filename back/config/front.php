<?php
//This framework tools
use back\tools\basic;
//=================================

//This class is intended to be the default route for the css and js file
class front extends main\s_r\request_condition{
    use main\s_r\server_config;
    
    //The parameter default value is '', if this happens then the request is not valid
    function __construct($param_req_res=''){
        parent::__construct();
        
        //Trigger the class basic from tools
        $b=new basic();

        //If user tries to access this directory without any query, don't allow them
        if($param_req_res==''){
            $b->error(403);
        }else{
            $limit=["r_method"=>["post","get"],"allowed_mime"=>["css","js"]];
            $b->setup($this)->uri_query_serve_resources($param_req_res, $limit, 1);
        }
        
    }
    
}
//====================================================================================
?>