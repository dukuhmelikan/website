<?php
namespace back\tools;

// Basic limiter is limiter that limit what bound to the protocol ex:request_method
class basic_limiter{
    private $default_limit=["r_method"=>["get"]];
    protected $data_config;
    protected $correct_condition=array();
    protected $mime_file;    
    protected $current_file_size;


    
    //=================================================
    function setup($data_config){
        $this->data_config=$data_config; // Requesting server_config and request_condition (accessible through config)
    
        return $this;
    }
    //=================================================

    // ================================================
    private function switch_error($err_code){
        switch($err_code){
            case 403:
                echo "Access forbidden";
                break;
            case 404:
                echo "Not Found";
                break;
            case 415:
                echo "Unsupported media";
                break;
            case 500:
                echo "Internal server error";
                break;
            default:
                echo "Error happened";
                break;
        }
    }
    //=================================================
    function error($err_code,$include_response_page=''){
        if($include_response_page!=''){
            if(!include($this->data_config->FILE_FRONT_VIEW.'/'.$include_response_page)){
                $this->switch_error($err_code);
            }
        }else{
            $this->switch_error($err_code);
        }
        http_response_code($err_code);
    }
    //=================================================    
    
    //=================================================
    function set_default_limit($limit){
        if($limit==''){
            $limit=$this->default_limit;
        }
        return $limit;
    }
    //=================================================
    
    //===============================================================================
    function limit($param_limit){
        /* Example of array

        $param_limit=[
            'allowed_mime'=>['ext'],
            'max_file_size'=>['1000000'],
            'max_file_size'=>['1000000'],
            'r_method'=>[post]
        ]
        
        */
        // Only used in case there is allowed mime
        $mime      = explode("=",$this->mime_file);
        $head_mime = $mime[0];
        $file_mime = $mime[1];
        $file_ext  = explode(".",$file_mime)[1];
        //=======================================
        
        $keys      = array_keys($param_limit);
        $number_of_keys = count($keys);
        // Setup the correct condition as 0 change in the next step
        for($i=0;$i<$number_of_keys;$i++){
            $this->correct_condition[$i]=0;
        }

        // Check does the request match with the limit
        for($x=0;$x<$number_of_keys;$x++){
            $key=$keys[$x];
            $items=$param_limit[$key];
            
            for($y=0;$y<count($items);$y++){
                $item=$items[$y];
                
                // Check the request method
                if($key=="r_method"){
                    if(strtolower($this->data_config->R_METHOD)==$item){
                        $this->correct_condition[$x]=1;
                    }
                }
                //==========================

                // Check the allowed mime
                if($key=="allowed_mime"){
                    if($head_mime==$item && $file_ext==$item){
                        $this->correct_condition[$x]=1;
                    }
                }
                //==========================

                // Check maximum size of file (in Bytes)
                if($key=="max_file_size"){
                    
                    if($this->current_file_size <= $item){
                        $this->correct_condition[$x]=1;
                    }
                    
                }
                //===========================

                // Check minimum size of file (in Bytes)
                if($key=="min_file_size"){
                    if($this->current_file_size >= $item){
                        $this->correct_condition[$x]=1;
                    }
                }
                //===========================
            }
        }

        return $this;

    }
    //============================================================================

    //============================================================================
    function execute_valid(){
        $wrong_point=0;
        for($i=0;$i<count($this->correct_condition);$i++){
            if($this->correct_condition[$i]==0){
                $wrong_point+=1;
            }
        }
            
        if($wrong_point>0){
            return false;
        }else{
            return true;
        }
    }
    //============================================================================
    
}

// Class basic
// To limit service
// To serve resources
// To help data receival
//====================================================================================
class basic extends basic_limiter{
    protected $R_DATA;

    //============================================================================
    // Request resources from URL
    // Ex: host/uri/?ext=file.ext
    // Ex: host/uri/sub?ext=file.ext
    function uri_query_serve_resources($uri_query_param, $limit='', $n=0){
        $limit=$this->set_default_limit($limit);

        $resources=explode("&",$uri_query_param);
        
        // Limit the number of request
        // If the parameter of n is filled only n resources will be answered
        // If no parameter of n is filled then all request will be answered 
        if($n==0){
            $number_request=count($resources);
        }else{
            $number_request=$n;
        }
        //=================================================================
        
        for($x=0;$x<$number_request;$x++){
            $this->mime_file    = $resources[$x];
            $this->limit($limit);
            $file               = explode('=',$this->mime_file)[1];
            $setup_mime_header  = explode('.', $file)[1];
        
            if($this->execute_valid()){
                header("Content-Type:text/".$setup_mime_header.";Charset:".$this->data_config->DEFAULT_CHARSET);
                //Currently only files in directory Front can be returned with this method
                $location=$this->data_config->FILE_FRONT_VIEW.'/'.$setup_mime_header.'/'.$file;
                
                if(file_exists($location)){
                    include($location);
                    return true;
                }else{
                    echo "File not exist";
                    return false;
                }

            }else{
                $this->error(403);
                return false;
            }
        }  
    }
    //=============================================================================

    // ============================================================================
    function serve_html_page($param_html_name, $limit=''){
        $limit=$this->set_default_limit($limit);

        $this->limit($limit);
        if($this->execute_valid()){
            $location=$this->data_config->FILE_FRONT_VIEW.'/'.$param_html_name;
            if(file_exists($location)){
                include($location);
                return true;
            }else{
                echo "File not exist";
                return false;
            }
        }else{
            $this->error(403);
            return false;
        }
    }
    //===============================================================================
    //===============================================================================
    // Take the data from request method of an URL
    function data_receive(){
        if($this->data_config->R_METHOD=="POST"){
            $this->R_DATA=$_POST;
        }else if($this->data_config->R_METHOD=="GET"){
            $this->R_DATA=$_GET;
        }
        
        return $this->R_DATA;
        
    }
    //===============================================================================
    //===============================================================================
    function file_upload($files, $limit=''){
        $limit=$this->set_default_limit($limit);
        $name=array_keys($files)[0];
        $file=$files[$name];
        $type=explode('/',$file['type'])[1]; // Explode ex:image/png -> png
        $exact_name=$file['name'];
        
        $this->mime_file=$type.'='.$exact_name; // For limiting the type
        $this->current_file_size=$file['size'];
        
        $this->limit($limit);
        if($this->execute_valid()){
            $target_file=$this->data_config->FILE_UPLOAD_DIR.'/'.basename($exact_name); // upload/filename.ext
            $move=move_uploaded_file($file['tmp_name'], $target_file);
            if($move){
                echo "Upload success";
                http_response_code(200);
                return true;
            }else{
                echo "Upload failed";
                $this->error(500);
                return false;
            }
        }else{
            $this->error(415);
            return false;
        }      
              
    }
    //==========================================================================================
    
    //==========================================================================================
    private function download($location){
        // Check file availability
            
        if(file_exists($location)){
            // Setup header
            header("Content-Description: File Transfer");
            header("Content-Type: application/octet-stream");
            header("Content-Disposition: attachment; filename=".basename($location));
            header("Expires: 0");
            header("Cache-Control: must-revalidate");
            header("Pragma: public");
            header("Content-Length: ".filesize($location));
            // Download file
            readfile($location);
            return true;
            flush();
            die();
        }else{
            $this->error(404);
            return false;
            exit();
        }
    }
    //===============================================================================================
    //===============================================================================================
    function files_download($file, $limit=''){      
        $limit=$this->set_default_limit($limit);
        $this->limit($limit);
        if($this->execute_valid()){
            $location=$this->data_config->FILE_DOWNLOAD_DIR.'/'.$file;             
            $this->download($location);
        }else{
            $this->error(403);
            return false;
            exit();
        }
            
    }
    //================================================================================================

}
//================================================================================================
?>