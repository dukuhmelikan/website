<?php
namespace back\tools;
class filemanip{

    function __construct($param_obj_config){
        $this->setup=$param_obj_config; // Requesting server_config and request_condition (accessible through config)
    }

    function FeedDataDB($file, $data){
        $file_location    = $this->setup->FILE_FRONT_VIEW.'/'.$file;
        $fpointer         = fopen($file_location, 'r');
        $filesize         = filesize($file_location);
        $rpointer         = fread($fpointer, $filesize);
        $chosen_data      = $data[0]; // choose only one data //update later
        $chosen_data_n    = count($chosen_data);
        $chosen_data_keys = array_keys($chosen_data);
        $mark_begin_ops   = '[';
        $mark_end_ops     = ']';
        $update = $rpointer;

        // taking the data into temporary memory
        for($x=0;$x<$chosen_data_n;$x++){
            $chosen_data_key            = $chosen_data_keys[$x];
            $chosen_data_key_length     = strlen($chosen_data_key);
            $datum                      = $chosen_data[$chosen_data_key];
            $target_replacement         = $mark_begin_ops.$chosen_data_key.$mark_end_ops;
            $target_replacement_length  = strlen($target_replacement);
           

            for($y=0;$y<$filesize;$y++){
                if($rpointer[$y]==$mark_begin_ops){
                    $z=0;
                    $temp_holder='';
                    while($rpointer[$y+$z-1]!=$mark_end_ops){
                        $temp_holder=$temp_holder.$rpointer[$y+$z];
                        $z++;
                    }
                    
                    if($target_replacement==$temp_holder){
                        $pointer_before='';
                        $pointer_after='';
                        for($i=0;$i<$y;$i++){
                            $pointer_before=$pointer_before.$rpointer[$i];
                        }
                        for($j=$y+$target_replacement_length;$j<$filesize;$j++){
                            $pointer_after=$pointer_after.$rpointer[$j];
                        }
                        $rpointer=$pointer_before.$datum.$pointer_after;
                    }
                }         
            }   
        }
        //return by echoing
        echo $rpointer;
    }
}
?>