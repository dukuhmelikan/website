<?php
include("other/server_config.php");
include("other/request_condition.php");

class routine extends main\s_r\request_condition{
    use main\s_r\server_config;

    protected $ARRAY_FILE_CONFIG;
    protected $array_config=array();
    protected $ARRAY_FILE_TOOLS;
    protected $array_tools;

    function __construct(){
        parent::__construct();
        header("charset=".$this->DEFAULT_CHARSET);
        $this->ARRAY_FILE_CONFIG = scandir($this->FILE_CONFIG);
        $this->ARRAY_FILE_TOOLS  = scandir($this->FILE_TOOLS);
    }

    private function make_error($error_code){
        switch($error_code){
            case 403:
                echo 'forbidden';
                break;
            case 404:
                echo 'not found';
                break;
            case 503:
                echo "Temporary Unavailable";
                break;
        }
        http_response_code($error_code);
    }

    private function check_uri_availability($param_uri){
        // This is the reason why the file and the class name it contains should be the same

        $split          = explode('/',$param_uri);
        $class_name     = explode('?',$split[0])[0];//explode again to separate if there is query
        $sub_class_name = explode('?',$split[1])[0];//explode again to separate if there is query
        $class_method   = get_class_methods($class_name);
        
        //To prevent from accessing more than sub class's relative
        if($split[2]!=null){
            return false;
        }else{
            //First condition where only the class name pointed in URL
            if($class_name!='' && $sub_class_name==''){
                //Check is this class exist as a config
                for($x=0;$x<count($this->array_config);$x++){
                    if($this->array_config[$x]==$class_name){
                        return true;
                    }
                }
                return false;
            }
            //Second condition where the class name and its method pointed in URL
            else if($class_name!='' && $sub_class_name!=''){
                //Check is this method exist as a sub config
                for($x=0;$x<count($class_method);$x++){
                    if($sub_class_name==$class_method[$x]){
                        return true;
                    }
                }
                return false;
            }

        }
        
    }
    function include_tools_files(){
        //Selecting tools files
        
        //First and default condition is to select all files
        //Disable by commenting if not using this condition
        //=====================================================================
        for($x=2;$x<count($this->ARRAY_FILE_TOOLS);$x++){
            include($this->FILE_TOOLS."/".$this->ARRAY_FILE_TOOLS[$x]);
        }
        //=====================================================================

        
        //Second condition is select some files(usable in case the website needs only some features)
        //===============================================================================
        /*$this->array_tools=array("tools_file_names");
        for($x=0;$x<count($this->array_tools);$x++){
            include($this->FILE_TOOLS."/".$this->array_tools[$x].$this->DEFAULT_EXT);
        }*/
        //===============================================================================
    }
    function include_config_files(){
        //Select all file in config directory
        //The $x start from 2 because by using scandir the . and .. are included
        
        for($x=2;$x<count($this->ARRAY_FILE_CONFIG);$x++){
            include($this->FILE_CONFIG.'/'.$this->ARRAY_FILE_CONFIG[$x]);
            $this->array_config[$x-2]=explode($this->DEFAULT_EXT,$this->ARRAY_FILE_CONFIG[$x])[0];
        }
    }
    

    function execute(){
       if($this->MAINTENANCE_MODE!=true){
            // Split the name of the website from the route
            $take_route=explode($this->SITE_FROM_HOST_URI, $this->R_URI, 2);
            
            // See where the client request
            // Distinguish bases on the link

            // First condition where the request points directly to the host URL
            if($take_route[1]==''){
                new main();
            }
            else{
                // Check if where the URL is pointing
                $check=$this->check_uri_availability($take_route[1]);
                
                // URL points to a configured place
                if($check==true){
                    $route                  = explode('/', $take_route[1]);
                    $class                  = $route[0];
                    $sub_class              = explode("?", $route[1])[0];
                    $class_holder;
                    $query_holder           = explode('?', $route[1])[1];
                    
                    //Second condition where the request points to sub URL with query
                    if($class!='' && $sub_class=='' && $query_holder!=''){
                        new $class($query_holder);
                    }
                    // Third condition where the request points to sub URL
                    else if($class!='' && $sub_class==''){
                        new $class();
                    }
                    // Fourth condition where the request points to sub URL's relative with query
                    else if($class!='' && $sub_class!='' && $query_holder!=''){
                        $class_holder=new $class();
                        $class_holder->$sub_class($query_holder);
                    }
                    // Fifth condition where the request points to sub URL's relative
                    else if($class!='' && $sub_class!=''){
                        $class_holder=new $class();
                        $class_holder->$sub_class();
                    }else{
                        $this->make_error(404);
                    }
                }
                // URL points to nowhere
                else{
                    $this->make_error(404);
                }
            }
       }
       //Maintenance mode
       else{
           $this->make_error(503);
       }
    
    }
}

$routine=new routine();
$routine->include_tools_files();
$routine->include_config_files();
$routine->execute();


?>
